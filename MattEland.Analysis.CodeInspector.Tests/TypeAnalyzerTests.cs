using System;
using MattEland.Analysis.CodeInspector.Core;
using MattEland.Analysis.CodeInspector.Core.Analyzers;
using Shouldly;
using Xunit;

namespace MattEland.Analysis.CodeInspector.Tests
{
    public class TypeAnalyzerTests
    {

        [Fact]
        public void InspectingAnalyzerHasInstructions()
        {
            // Arrange
            Type t = typeof(TypeAnalyzer);

            // Act
            var results = t.Analyze();

            // Assert
            results.ShouldNotBeNull();
            results.InstructionCount.ShouldBeGreaterThan(0);
        }
    }
}
