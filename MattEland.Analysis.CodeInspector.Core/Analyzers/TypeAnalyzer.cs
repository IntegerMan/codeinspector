﻿using System;
using MattEland.Analysis.CodeInspector.Core.Analyzers.Results;
using MattEland.Shared.Collections;

namespace MattEland.Analysis.CodeInspector.Core.Analyzers
{
    public class TypeAnalyzer
    {
        private readonly Type _type;

        private TypeAnalysisResults _analysis;

        public TypeAnalyzer(Type type)
        {
            _type = type;
        }

        public TypeAnalysisResults Analyze()
        {
            _analysis = new TypeAnalysisResults(_type.FullName);

            AnalyzeNestedTypes();
            AnalyzeMethods();

            return _analysis;
        }

        private void AnalyzeMethods()
        {
            _type.GetMethods().Each(m =>
            {
                var results = m.Analyze();

                _analysis.Add(results);
            });
        }

        private void AnalyzeNestedTypes() => _type.GetNestedTypes().Each(nested => _analysis.Add(nested.Analyze()));
    }
}
