﻿using System;
using ClrTest.Reflection;
using JetBrains.Annotations;
using MattEland.Analysis.CodeInspector.Core.Analyzers.Results;

namespace MattEland.Analysis.CodeInspector.Core.Analyzers
{
    public class InstructionAnalyzer
    {
        public static InstructionResult Analyze([NotNull] ILInstruction instruction)
        {
            if (instruction == null) throw new ArgumentNullException(nameof(instruction));

            var name = GetInstructionName(instruction);

            return new InstructionResult(name) { Offset = instruction.Offset};
        }

        private static string GetInstructionName([NotNull] ILInstruction instruction)
        {
            switch (instruction)
            {
                case InlineNoneInstruction noop:
                    return noop.OpCode.ToString();

                case InlineMethodInstruction call:
                    return $"{call.OpCode} {call.Method.Name}"; // TODO: What if it's in a different type?s

                case ShortInlineBrTargetInstruction brTarget:
                    return "branch to " + brTarget.TargetOffset; // TODO: On what condition?

                default:
                    return $"{instruction.OpCode} {instruction.GetType().Name}";
            }
        }
    }
}