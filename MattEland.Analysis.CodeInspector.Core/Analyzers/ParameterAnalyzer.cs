﻿using System.Reflection;
using MattEland.Analysis.CodeInspector.Core.Analyzers.Results;

namespace MattEland.Analysis.CodeInspector.Core.Analyzers
{
    public class ParameterAnalyzer
    {
        private readonly ParameterInfo _parameter;

        public ParameterAnalyzer(ParameterInfo parameter)
        {
            _parameter = parameter;
        }
        
        public ParameterAnalysisResults Analyze() => 
            new ParameterAnalysisResults(_parameter.Name) {Type = _parameter.ParameterType.FullName};
    }
}