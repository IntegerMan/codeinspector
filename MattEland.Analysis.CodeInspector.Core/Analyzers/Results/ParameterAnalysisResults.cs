﻿namespace MattEland.Analysis.CodeInspector.Core.Analyzers.Results
{
    public class ParameterAnalysisResults : AnalysisResultsBase
    {
        public ParameterAnalysisResults(string name) : base(name)
        {
        }


        public string Type { get; set; }
    }
}