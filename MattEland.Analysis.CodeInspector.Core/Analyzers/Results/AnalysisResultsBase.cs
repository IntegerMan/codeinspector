﻿namespace MattEland.Analysis.CodeInspector.Core.Analyzers.Results
{
    public abstract class AnalysisResultsBase
    {
        protected AnalysisResultsBase(string name)
        {
            Name = name;
        }

        public string Name { get; }
        public int InstructionCount { get; protected internal set; }
        public MemberVisibility Visibility { get; set; } = MemberVisibility.Public;
    }
}