﻿using System.Collections.Generic;

namespace MattEland.Analysis.CodeInspector.Core.Analyzers.Results
{
    public class TypeAnalysisResults : AnalysisResultsBase
    {
        public TypeAnalysisResults(string name) : base(name)
        {
        }

        public List<MethodAnalysisResults> Methods { get; private set; }

        public List<TypeAnalysisResults> Types { get; private set; }

        public int MethodCount { get; private set; }

        public void Add(MethodAnalysisResults results)
        {
            if (Methods == null)
            {
                Methods = new List<MethodAnalysisResults>();
            }
            Methods.Add(results);

            InstructionCount += results.InstructionCount;
            MethodCount++;
        }

        public void Add(TypeAnalysisResults innerClassResults)
        {
            if (Types == null)
            {
                Types = new List<TypeAnalysisResults>();
            }
            Types.Add(innerClassResults);
        }
    }
}