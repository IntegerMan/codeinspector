﻿using System.Collections.Generic;

namespace MattEland.Analysis.CodeInspector.Core.Analyzers.Results
{
    public class MethodAnalysisResults : AnalysisResultsBase
    {
        public MethodAnalysisResults(string name) : base(name)
        {
        }

        public int ParameterCount { get; private set; }
        public bool IsStatic { get; set; }

        public List<ParameterAnalysisResults> Parameters { get; private set; }

        public List<InstructionResult> Instructions { get; private set; }

        public void Add(ParameterAnalysisResults parameterAnalysisResults)
        {
            if (Parameters == null)
            {
                Parameters = new List<ParameterAnalysisResults>();
            }
            Parameters.Add(parameterAnalysisResults);

            ParameterCount++;
        }

        public void Add(InstructionResult instruction)
        {
            if (Instructions == null)
            {
                Instructions = new List<InstructionResult>();
            }
            Instructions.Add(instruction);

            InstructionCount++;
        }
    }
}