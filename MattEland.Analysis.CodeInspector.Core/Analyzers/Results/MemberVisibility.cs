﻿namespace MattEland.Analysis.CodeInspector.Core.Analyzers.Results
{
    public enum MemberVisibility
    {
        Public,
        Private,
        Protected
    }
}