﻿namespace MattEland.Analysis.CodeInspector.Core.Analyzers.Results
{
    public class InstructionResult
    {
        public InstructionResult(string name)
        {
            Name = name;
        }

        public string Name { get; }
        public int Offset { get; set; }
    }
}