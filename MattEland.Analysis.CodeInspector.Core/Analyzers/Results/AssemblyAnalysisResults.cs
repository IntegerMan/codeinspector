﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace MattEland.Analysis.CodeInspector.Core.Analyzers.Results
{
    public class AssemblyAnalysisResults : AnalysisResultsBase
    {
        public AssemblyAnalysisResults(string name) : base(name)
        {
        }

        public List<TypeAnalysisResults> Types { get; private set; }

        public void Add([NotNull] TypeAnalysisResults results)
        {
            if (results == null) throw new ArgumentNullException(nameof(results));

            if (Types == null)
            {
                Types = new List<TypeAnalysisResults>();
            }
            Types.Add(results);

            InstructionCount += results.InstructionCount;
        }
    }
}