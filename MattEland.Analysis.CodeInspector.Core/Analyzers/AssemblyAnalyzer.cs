﻿using System;
using System.Reflection;
using JetBrains.Annotations;
using MattEland.Shared.Collections;
using MattEland.Analysis.CodeInspector.Core.Analyzers.Results;

namespace MattEland.Analysis.CodeInspector.Core.Analyzers
{
    public class AssemblyAnalyzer
    {
        private readonly Assembly _assembly;

        public AssemblyAnalyzer([NotNull] Assembly assembly)
        {
            _assembly = assembly ?? throw new ArgumentNullException(nameof(assembly));
        }

        public AssemblyAnalysisResults Analyze()
        {
            var results = new AssemblyAnalysisResults(_assembly.FullName);

            var types = _assembly.GetTypes();
            types.Each(t => results.Add(t.Analyze()));

            return results;
        }
    }
}