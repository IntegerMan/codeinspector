﻿using System;
using System.Reflection;
using ClrTest.Reflection;
using JetBrains.Annotations;
using MattEland.Analysis.CodeInspector.Core.Analyzers.Results;

namespace MattEland.Analysis.CodeInspector.Core.Analyzers
{
    public static class AnalysisExtensions
    {
        public static MethodAnalysisResults Analyze([NotNull] this MethodInfo method)
        {
            if (method == null) throw new ArgumentNullException(nameof(method));

            var analyzer = new MethodAnalyzer(method);

            return analyzer.Analyze();
        }

        public static TypeAnalysisResults Analyze([NotNull] this Type type)
        {
            if (type == null) throw new ArgumentNullException(nameof(type));

            var analyzer = new TypeAnalyzer(type);

            return analyzer.Analyze();
        }

        public static AssemblyAnalysisResults Analyze([NotNull] this Assembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException(nameof(assembly));

            var analyzer = new AssemblyAnalyzer(assembly);

            return analyzer.Analyze();
        }

        public static ParameterAnalysisResults Analyze([NotNull] this ParameterInfo parameter)
        {
            if (parameter == null) throw new ArgumentNullException(nameof(parameter));

            var analyzer = new ParameterAnalyzer(parameter);

            return analyzer.Analyze();
        }

        public static InstructionResult Analyze([NotNull] this ILInstruction instruction)
        {
            if (instruction == null) throw new ArgumentNullException(nameof(instruction));

            return InstructionAnalyzer.Analyze(instruction);
        }
    }
}