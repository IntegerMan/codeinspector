﻿using System.Linq;
using System.Reflection;
using ClrTest.Reflection;
using MattEland.Analysis.CodeInspector.Core.Analyzers.Results;
using MattEland.Shared.Collections;

namespace MattEland.Analysis.CodeInspector.Core.Analyzers
{
    public class MethodAnalyzer
    {
        private readonly MethodInfo _method;
        private MethodAnalysisResults _analysis;

        public bool IncludeInstructions { get; set; } = true;

        public MethodAnalyzer(MethodInfo method)
        {
            _method = method;
        }

        public MethodAnalysisResults Analyze()
        {
            _analysis = new MethodAnalysisResults(_method.Name)
            {
                IsStatic = _method.IsStatic,
                Visibility = GetVisibility(_method)
            };

            AnalyzeInstructions();
            AnalyzeParameters();

            return _analysis;
        }

        private void AnalyzeParameters() => _method.GetParameters().Each(p => _analysis.Add(p.Analyze()));

        private void AnalyzeInstructions()
        {
            var reader = new ILReader(_method);

            if (IncludeInstructions)
            {
                reader.Each(instruction => _analysis.Add(instruction.Analyze()));
            }
            else
            {
                _analysis.InstructionCount = reader.Count();
            }
        }

        public static MemberVisibility GetVisibility(MethodInfo info)
        {
            if (info.IsPrivate) return MemberVisibility.Private;

            if (info.IsPublic) return MemberVisibility.Public;

            return MemberVisibility.Protected; // TODO: What about the other types of visibility?
        }

    }
}