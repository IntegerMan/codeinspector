﻿using System;
using MattEland.Analysis.CodeInspector.Core;
using MattEland.Analysis.CodeInspector.Core.Analyzers;
using Newtonsoft.Json;

namespace MattEland.Analysis.CodeInspector.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var result = typeof(MethodAnalyzer).Assembly.Analyze();

            var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
            });

            System.Console.WriteLine(json);
        }
    }

}
